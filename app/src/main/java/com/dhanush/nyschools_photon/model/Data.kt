package com.dhanush.nyschools_photon.model

data class School(
    @SerializedName("school_name")
    val schoolName:String?,
    
    @SerializedName("dbn")
    val dbn: String?)


